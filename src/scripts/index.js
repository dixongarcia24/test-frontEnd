"use strict";
$(document).ready(function($) {
    $('.alert').hide();
});

$('#btn-enviar').click(function(event) {
    var valido = true;
    $.each($('form').find('.form-group'), function(index, val) {
        var input = $(this).find(':input');
        if (input.val() == '') {
            input.addClass('alert-danger');
            valido = false;
        }
    });
    if (valido) {
        var nombre = $('#nombre').val();
        var correo = $('#correo').val();
        if (isEmail(correo)) {
            //archivo con plantilla de mail
            var contenidoHTML = '';
            $.get('../../mail.html').then(function(response) {
                contenidoHTML = response;
            });
            $.ajax({
                url: 'https://ruta/archivo/php/envio/email',
                type: 'POST',
                dataType: 'json',
                data: {
                    message: {
                        html: contenidoHTML,
                        subject: 'Descubre lo nuevo en Pleisi',
                        from_email: 'info@pleisi.com',
                        from_name: 'Pleisi',
                        to: [{
                            email: correo,
                            name: nombre,
                            type: 'to'
                        }],
                        header: {
                            'Reply-To': 'info@pleisi.com'
                        }

                    }
                },
                complete: function(xhr, textStatus) {
                    //called when complete
                },
                success: function(data, textStatus, xhr) {
                    //called when successful
                },
                error: function(xhr, textStatus, errorThrown) {
                    //called when there is an error
                }
            });
        } else {
            $('#correo').addClass('alert-warning');
            $('form').prepend('<div class="alert alert-warning alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true"> &times; </span></button> <strong>Advertencia!</strong > Debe ingresar una dirección de correo valida. </div > ');
        }
    } else {
        $('form').prepend('<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true"> &times; </span></button> <strong>¡Error!</strong > Alguno de los campos está vacío. </div > ');
    }
});

$(':input').focusin(function(event) {
    $(this).removeClass('alert-danger');
    $(this).removeClass('alert-warning');
});

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}