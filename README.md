# testFrontEnd

> Prueba FrontEnd Developer para Abako

## Technologies

- [**Gulp**](http://gulpjs.com)
- [**Jade**](http://jade-lang.com) 
- [**Sass**](http://sass-lang.com)  
- [**JSHint**](http://jshint.com) 

## Install and Use

### Install

```bash
npm install -g testFrontEnd
```

### Use 

```bash
yo testFrontEnd
```

# Author 

Dixon Garcia - dixongarcia24@gmail.com

# License 

The code is available under the **MIT** license. 
